public class FizzBuzzRecursion {
public static void main (String[] args) {
        count(1);
    }
public static void count(int i) {
        if (i >= 101) {
            return;
        }
        if (i % 15 == 0) {
            System.out.println("FizzBuzz");
        }
        else if (i % 5 == 0) {
            System.out.println("Buzz");
        }
        else if (i % 3 == 0) {
            System.out.println("Fizz");
        }
        else {
            System.out.println(i);
        }
        count(i + 1);
    }
}
